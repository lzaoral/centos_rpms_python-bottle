#!/bin/bash

# exit immediately if any command returns non-zero exit code
set -e

# print commands as they are executed by the shell interpreter
set -x

# download source RPM of python-pycurl
yum download --source python-pycurl

# rebuild the source RPM (%check uses bottle)
rpmbuild --rebuild ./python-pycurl-*.src.rpm
