#!/bin/bash

# exit immediately if any command returns non-zero exit code
set -e

# print commands as they are executed by the shell interpreter
set -x

# global constants
HOST="localhost"
PORT="1234"
URL="http://${HOST}:${PORT}/hello/rhel"
CURL_OUT="./curl.out"
CURL_ERR="./curl.err"

# print versions of related pkgs
PKGS="$(set +x; eval echo {lib,}curl python3-bottle)"
rpm -q $PKGS | sort -V
rpm -V $PKGS

# run HTTP server in the background
./hello.py &
BOTTLE_PID=$!

# FIXME: wait for open port instead
sleep 2

# check that HTTP server works using curl
curl -fsvo $CURL_OUT $URL

# check whether the received data matches the expected contents
diff <(printf "<b>Hello rhel</b>!") $CURL_OUT

# kill nghttpd running in the background
kill $BOTTLE_PID

# wait till the background process finishes
wait
